package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    
    @Test
    public void addTest() {
        assertEquals(2, Calculator.add(1, 1));
    }

    @Test
    public void subtractTest() {
        assertEquals(2, Calculator.subtract(4, 2));
    }

    @Test
    public void multiplyTest() {
        assertEquals(8, Calculator.multiply(2, 4));
    }

    @Test
    public void divideTest() {
        assertEquals(2, Calculator.divide(4, 2));
    }

    @Test
    public void testDivideByZero() {
        assertThrows(ArithmeticException.class, () -> {
            Calculator.divide(10, 0);
        });
    }
}