package cz.cvut.fel.ts1;

public class Krizste2 {
    public static long factorial (int n) {
        long factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }

    // Rekurzivní metoda pro výpočet faktoriálu
    public static long factorialRec(int n) {
        // Base case: faktoriál 0 nebo 1 je 1
        if (n == 0 || n == 1) {
            return 1;
        }
        // Rekurzivní krok: n! = n * (n-1)!
        return n * factorialRec(n - 1);
    }
}