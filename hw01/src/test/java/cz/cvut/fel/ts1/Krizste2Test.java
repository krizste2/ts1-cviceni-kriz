package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Krizste2Test {
    @Test
    public void factorialTest () {
        assertEquals(1, Krizste2.factorial(0));
        assertEquals(1, Krizste2.factorial(1));
        assertEquals(2, Krizste2.factorial(2));
        assertEquals(6, Krizste2.factorial(3));
        assertEquals(24, Krizste2.factorial(4));
        assertEquals(120, Krizste2.factorial(5));
        assertEquals(720, Krizste2.factorial(6));
        assertEquals(5040, Krizste2.factorial(7));
    }

    @Test
    public void factorialRecursiveTest() {
        assertEquals(1, Krizste2.factorialRec(0));
        assertEquals(1, Krizste2.factorialRec(1));
        assertEquals(2, Krizste2.factorialRec(2));
        assertEquals(6, Krizste2.factorialRec(3));
        assertEquals(24, Krizste2.factorialRec(4));
        assertEquals(120, Krizste2.factorialRec(5));
        assertEquals(720, Krizste2.factorialRec(6));
        assertEquals(5040, Krizste2.factorialRec(7));
    }
}